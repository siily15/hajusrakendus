<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\Map;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\Weatherapi;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/blog', [BlogController::class, 'index']);
Route::post('/comments', [CommentsController::class, 'index']);
Route::post('/comment/{blog}', [CommentsController::class, 'store']);
Route::post('/comment-delete/{comment}', [CommentsController::class, 'delete'])->middleware('role:Admin');


Route::get('/weather', [Weatherapi::class, 'weather'])->name('weather');

Route::get('/map', [Map::class, 'index'])->name("map");
Route::post('/map', [Map::class, 'store'])->name('add.marker');
Route::post('/map/marker/{id}', [Map::class, 'update'])->name("update.marker");
Route::get('/map/marker/{id}', [Map::class, 'show'])->name('edit.marker');
Route::delete('/map/{id}', [Map::class, 'delete'])->name('delete.marker');

Route::get('/store', [ShopController::class, 'index'])->name('store');
Route::post('/cart', [ShopController::class, 'cartAdd'])->name('add.cart');
Route::get('/cart', [ShopController::class, 'CartList'])->name('cart.list');
Route::put('/cart/{id}', [ShopController::class, 'cartUpdate'])->name('update.cart');
Route::delete('/cart/{id}', [ShopController::class, 'destroy'])->name('delete.cart');

Route::get('/stripeCart', [ShopController::class, 'checkoutData'])->name('get.stripeCart');

Route::get('/success', [ShopController::class, 'success']);

require __DIR__ . '/auth.php';
